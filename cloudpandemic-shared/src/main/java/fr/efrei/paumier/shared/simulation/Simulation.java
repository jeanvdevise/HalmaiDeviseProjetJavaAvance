package fr.efrei.paumier.shared.simulation;

import fr.efrei.paumier.shared.time.TimeManager;

public interface Simulation extends TimeManager {

	int getOriginalPopulation();
	int getHealthyPopulation();
	int getInfectedPopulation();
	int getIsolatedPopulation();
	int getEncryptedPopulation();
	
}
