package fr.efrei.halmai.devise.projet.test.simulation;

import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.simulation.BaseSimulationTests;
import fr.efrei.paumier.shared.simulation.Simulation;
import fr.efrei.paumier.shared.time.FakeClock;

public class SimulationTest extends BaseSimulationTests {

    @Override
    protected Simulation createSimulation(FakeClock clock, Selector selector, int population) {
        return new BaseSimulation(clock,selector,population);
    }
}

