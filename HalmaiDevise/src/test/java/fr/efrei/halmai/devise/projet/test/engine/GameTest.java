package fr.efrei.halmai.devise.projet.test.engine;

import fr.efrei.halmai.devise.projet.engine.BaseGame;
import fr.efrei.halmai.devise.projet.server.ServerConnection;
import fr.efrei.halmai.devise.projet.service.SelectorService;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.engine.BaseGameEngineTests;
import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.time.FakeClock;

public class GameTest extends BaseGameEngineTests {
    @Override
    protected GameEngine createGameEngine(FakeClock clock) {
        return new BaseGame(new ServerConnection(new BaseSimulation(clock, new SelectorService(), 0)), new BaseSimulation(clock, new SelectorService(), 0), clock);
    }


}
