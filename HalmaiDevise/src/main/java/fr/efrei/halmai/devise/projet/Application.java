package fr.efrei.halmai.devise.projet;

import fr.efrei.halmai.devise.projet.engine.BaseGame;
import fr.efrei.halmai.devise.projet.gui.JFrameGui;
import fr.efrei.halmai.devise.projet.server.ServerConnection;
import fr.efrei.halmai.devise.projet.service.SelectorService;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;

import javax.swing.*;
import java.io.IOException;
import java.time.Clock;

public class Application {

    public static void main(String[] args) throws InterruptedException, IOException {

        SelectorService selector = new SelectorService();
        Clock clock = Clock.systemDefaultZone();

        BaseSimulation simulation = new BaseSimulation(clock, selector,100);
        ServerConnection serverConnection = new ServerConnection(simulation);
        serverConnection.connecting();
        BaseGame baseGame = new BaseGame(serverConnection, simulation,clock);

        UpdatePlague update = new UpdatePlague(baseGame, simulation);
        DisplayPlague display = new DisplayPlague(baseGame);

        update.start();
        display.start();

        update.join();
        display.join();
    }

}

class UpdatePlague extends Thread {

    private BaseGame engine;
    private BaseSimulation simulation;

    UpdatePlague(BaseGame baseGame, BaseSimulation sim) {
        this.engine = baseGame;
        this.simulation = sim;
    }

    @Override
    public void run() {
        System.out.println("** Debut de l'epidemie **");
        try {
            updateLoop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void updateLoop() throws InterruptedException {
        while (simulation.getInfectedPopulation() != simulation.getOriginalPopulation()) {
            simulation.update();
            engine.update();
            sleep(200);
        }
    }
}

class DisplayPlague extends Thread{

    private JFrameGui frame;
    private BaseGame engine;
    private BaseSimulation simulation;

    DisplayPlague(BaseGame engine){
        this.engine = engine;
        this.simulation = engine.getSimulation();
        frame = new JFrameGui(engine);
    }

    @Override
    public void run() {
        try {
            displayLoop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void displayLoop() throws InterruptedException {
        while (true){
            frame.updateStats();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
            sleep(1000);
        }
    }
}
