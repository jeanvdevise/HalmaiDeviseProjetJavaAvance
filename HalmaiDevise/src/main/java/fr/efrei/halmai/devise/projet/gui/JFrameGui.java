/*
 * Created by JFormDesigner on Sun May 30 12:19:07 CEST 2021
 */

package fr.efrei.halmai.devise.projet.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import fr.efrei.halmai.devise.projet.engine.BaseGame;
import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.service.Status;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import lombok.Getter;

/**
 * @author unknown
 */
public class JFrameGui extends JFrame {
    
    private BaseGame engine;
    private BaseSimulation simulation;
    
    public JFrameGui(BaseGame engine) {
        this.engine = engine;
        this.simulation = engine.getSimulation();
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Jean Devise
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        label9 = new JLabel();
        label5 = new JLabel();
        tabbedPane1 = new JTabbedPane();
        scrollPane1 = new JScrollPane();
        scrollPane2 = new JScrollPane();
        table1 = new JTable(new ComputerTableModel(simulation.getComputerList()));
        label10 = new JLabel();
        button2 = new JButton();
        label12 = new JLabel();
        button3 = new JButton();
        comboBox1 = new JComboBox(comboboxList);
        button4 = new JButton();
        action2 = new UpgradeOffice();
        execute = new ExecuteAction();
        console = new Console(new JTextArea(10, 4));
        actionUpgradeProtection = new UpgradeProtection();

        PrintStream printStream = new PrintStream(console);
        System.setOut(printStream);
        System.setErr(printStream);

        //======== this ========
        setTitle("Plague Project Halmai Devise");
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(Borders.createEmptyBorder("7dlu, 7dlu, 7dlu, 7dlu"));
            dialogPane. addPropertyChangeListener( new java. beans .PropertyChangeListener ( )
            { @Override public void propertyChange (java . beans. PropertyChangeEvent e) { if( "\u0062order" .equals ( e. getPropertyName () ) )
            throw new RuntimeException( ) ;} } );
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new FormLayout(
                    "67dlu, 5*($lcgap, 60dlu)",
                    "3*(default, $lgap), 0dlu, 5*($lgap, default), $lgap, 10dlu"));

                //---- label1 ----
                label1.setText("Fonctionnels");
                contentPanel.add(label1, CC.xy(3, 1));

                //---- label2 ----
                label2.setText("Infect\u00e9s");
                contentPanel.add(label2, CC.xy(5, 1));

                //---- label3 ----
                label3.setText("Isol\u00e9s");
                contentPanel.add(label3, CC.xy(7, 1));

                //---- label4 ----
                label4.setText("Chiffr\u00e9s");
                contentPanel.add(label4, CC.xywh(9, 1, 1, 2));

                //---- label9 ----
                label9.setText("Argent");
                contentPanel.add(label9, CC.xy(11, 1));

                //---- label5 ----
                label5.setText("Total");
                contentPanel.add(label5, CC.xy(1, 3));

                //======== tabbedPane1 ========
                {

                    //======== scrollPane1 ========
                    {

                        //---- table1 ----
                        table1.setPreferredScrollableViewportSize(new Dimension(450, 700));
                        scrollPane1.setViewportView(table1);
                    }
                    {
                        console.getOutput().setSize(new Dimension(450, 700));
                        scrollPane2.setSize(new Dimension(450, 700));
                        scrollPane2.setViewportView(console.getOutput());
                    }
                    tabbedPane1.addTab("Details", scrollPane1);
                    tabbedPane1.addTab("Evénements", scrollPane2);

                }
                contentPanel.add(tabbedPane1, CC.xywh(1, 5, 11, 1));

                //---- label10 ----
                label10.setText("Niveau bureautique");
                contentPanel.add(label10, CC.xy(1, 9));

                //---- button2 ----
                button2.setText("Upgrade 50\u20ac");
                button2.setAction(action2);
                button2.setActionCommand("Upgrade");
                contentPanel.add(button2, CC.xy(3, 9));

                //---- label12 ----
                label12.setText("Niveau Antivirus");
                contentPanel.add(label12, CC.xy(1, 13));

                //---- button3 ----
                button3.setText("Upgrade 50\u20ac");
                button3.setAction(actionUpgradeProtection);
                button3.setActionCommand("Upgrade");
                contentPanel.add(button3, CC.xy(3, 13));
                contentPanel.add(comboBox1, CC.xywh(5, 15, 5, 1));

                //---- button4 ----
                button4.setAction(execute);
                contentPanel.add(button4, CC.xy(11, 15));
            }
            dialogPane.add(contentPanel, BorderLayout.NORTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public void updateStats(){

        table1.updateUI();
        simulation.updateStats();

        labelHealthy.setText(String.valueOf(simulation.getHealthyPopulation()));
        contentPanel.add(labelHealthy, CC.xy(3, 3));
        
        labelInfected.setText(String.valueOf(simulation.getInfectedPopulation()));
        contentPanel.add(labelInfected, CC.xy(5, 3));
        
        labelIsolated.setText(String.valueOf(simulation.getIsolatedPopulation()));
        contentPanel.add(labelIsolated, CC.xy(7, 3));
        
        labelEncrypted.setText(String.valueOf(simulation.getEncryptedPopulation()));
        contentPanel.add(labelEncrypted, CC.xy(9, 3));
        
        labelMoney.setText(String.valueOf(simulation.getMoney()));
        contentPanel.add(labelMoney, CC.xy(11, 3));

        labelOffice.setText(String.valueOf(simulation.getOfficeLevel()));
        contentPanel.add(labelOffice, CC.xy(1, 11));

        labelProtection.setText(String.valueOf(simulation.getProtectionLevel()));
        contentPanel.add(labelProtection, CC.xy(1, 15));

    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Jean Devise
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label9;
    private JLabel label5;
    private JTabbedPane tabbedPane1;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JTable table1;
    private JLabel label10;
    private JButton button2;
    private JLabel label12;
    private JButton button3;
    private JComboBox comboBox1;
    private JButton button4;
    private UpgradeOffice action2;
    private ExecuteAction execute;
    private Console console;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private UpgradeProtection actionUpgradeProtection;
    private JLabel labelHealthy = new JLabel();
    private JLabel labelInfected = new JLabel();
    private JLabel labelIsolated = new JLabel();
    private JLabel labelEncrypted = new JLabel();
    private JLabel labelMoney = new JLabel();
    private JLabel labelOffice = new JLabel();
    private JLabel labelProtection = new JLabel();
    private String[] comboboxList = {"Formater un ordinateur (60s)", "Déchiffrer un oridnateur (100€)"};

    private class UpgradeOffice extends AbstractAction {
        private UpgradeOffice() {
            // JFormDesigner - Action initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
            // Generated using JFormDesigner Evaluation license - Jean Devise
            putValue(NAME, "Upgrade 50\u20ac");
            // JFormDesigner - End of action initialization  //GEN-END:initComponents
        }

        public void actionPerformed(ActionEvent e) {
            engine.officeLevelIncrementEventRegister();
        }
    }

    private class UpgradeProtection extends AbstractAction {
        private UpgradeProtection() {
            putValue(NAME, "Upgrade 800\u20ac");
            // JFormDesigner - End of action initialization  //GEN-END:initComponents
        }

        public void actionPerformed(ActionEvent e) {
            engine.protectionLevelIncrementEventRegister();
        }
    }
    

    private class ExecuteAction extends AbstractAction {
        private ExecuteAction() {
            // JFormDesigner - Action initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
            // Generated using JFormDesigner Evaluation license - Jean Devise
            putValue(NAME, "Execute");
            // JFormDesigner - End of action initialization  //GEN-END:initComponents
        }

        public void actionPerformed(ActionEvent e) {
            if(comboBox1.getSelectedIndex()==0){
                engine.formattingEventRegister();
            }
            else {
                engine.decipherEventRegister();
            }
        }
    }
}


class ComputerTableModel extends AbstractTableModel{

    private List<Computer> computerList;

    ComputerTableModel(List<Computer> computerList){
        this.computerList = computerList;
    }

    @Override
    public String getColumnName(int columnIndex){
        if (columnIndex == 0){
            return "id";
        }
        if (columnIndex == 1){
            return "Santé";
        }
        if (columnIndex == 2){
            return "Connection";
        }
        return null;
    }

    @Override
    public int getRowCount() {
        return computerList.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Computer computer = computerList.get(rowIndex);
        if(columnIndex == 0){
            return computer.getId();
        }

        if(columnIndex == 1){
            if (computer.getStatus() == Status.HEALTHY){
                return "Sain";
            }
            else if (computer.getStatus() == Status.ISOLATED){
                if (computer.isComputerEncrypted()){
                    return "Chiffré";
                }
                return "Isolé";
            }
            else if(computer.getStatus() == Status.INFECTED){
                if (computer.isComputerEncrypted()){
                    return "Chiffré";
                }
                return "Infecté";
            }
        }

        if(columnIndex == 2){
            if (computer.getStatus() == Status.ISOLATED){
                return "Isolé";
            }
            else{
                return "Connecté";
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void fireTableDataChanged() {
        fireTableChanged(new TableModelEvent(this));
    }

}

@Getter
class Console extends OutputStream {

    /**
     * Represents the data written to the stream.
     */
    ArrayList <Byte> data = new ArrayList <Byte> ();

    /**
     * Represents the text area that will be showing the written data.
     */
    private JTextArea output;

    /**
     * Creates a console context.
     * @param output
     *      The text area to output the consoles text.
     */
    public Console(JTextArea output) {
        this.output = output;
    }

    /**
     * Called when data has been written to the console.
     */
    private void fireDataWritten() {

        // First we loop through our written data counting the lines.
        int lines = 0;
        for (int i = 0; i < data.size(); i++) {
            byte b = data.get(i);

            // Specifically we look for 10 which represents "\n".
            if (b == 10) {
                lines++;
            }

            // If the line count exceeds 250 we remove older lines.
            if (lines >= 250) {
                data = (ArrayList<Byte>) data.subList(i, data.size());
            }
        }

        // We then create a string builder to append our text data.
        StringBuilder bldr = new StringBuilder();

        // We loop through the text data appending it to the string builder.
        for (byte b : data) {
            bldr.append((char) b);
        }

        // Finally we set the outputs text to our built string.
        output.setText(bldr.toString());
    }

    @Override
    public void write(int i) throws IOException {

        // Append the piece of data to our array of data.
        data.add((byte) i);

        // Indicate that data has just been written.
        fireDataWritten();
    }

}