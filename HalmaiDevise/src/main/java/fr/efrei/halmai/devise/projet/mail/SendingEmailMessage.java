package fr.efrei.halmai.devise.projet.mail;

import java.io.Serializable;

public class SendingEmailMessage implements Serializable {
    private final boolean infectedFlag;

    public SendingEmailMessage(boolean isInfected) {
        this.infectedFlag = isInfected;
    }

    public boolean isInfected() {
        return this.infectedFlag;
    }
}