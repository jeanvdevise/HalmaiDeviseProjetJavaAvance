package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.service.EventService;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Clock;
import java.time.Duration;

public class DecipherEvent implements Event {

    private BaseSimulation simulation;
    private EventService eventService;

    public DecipherEvent(BaseSimulation simulation) {
        this.simulation = simulation;
        this.eventService = new EventService(simulation);
    }

    @Override
    public void trigger() {
        if (simulation.getMoney() >= 100){
            if(eventService.setEncryptedIsolatedComputerHealthy()){
                simulation.setMoney(simulation.getMoney()-100);
            }
        }
        else if (simulation.getMoney()<100){
            System.out.println("Insufficient money to decipher the computer");
        }
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }

    public boolean isDurationOk(){
        return true;
    }

}
