package fr.efrei.halmai.devise.projet.server;

import fr.efrei.halmai.devise.projet.mail.ReceivingEmailMessage;
import fr.efrei.halmai.devise.projet.mail.SendingEmailMessage;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.selection.Selector;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerConnection implements Runnable {

    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private Socket socket;
    private ReceivingEmailMessage messageReceived;
    private final BaseSimulation simulation;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private boolean isConnected;
    private Thread smtpServerThread;

    public ServerConnection(BaseSimulation simulation) {
        this.socket = new Socket();
        this.simulation = simulation;
        isConnected = false;
    }

    public void start() {
        smtpServerThread = new Thread(this);
        smtpServerThread.start();
    }

    public void connecting() throws IOException {
        this.socket = new Socket();
        socket.connect(new InetSocketAddress("138.68.177.223", 4242));
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());
        sendingHelloMessage();
        this.start();
        isConnected = true;
    }

    public void close() throws IOException {
        running.set(false);
        if (outputStream != null && inputStream != null) {
            outputStream.close();
            inputStream.close();
            socket.close();
        }
        isConnected = false;
    }

    public void sendingHelloMessage() {
        try {
            outputStream.writeObject("Hello");
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.err.println("OutputStream was not instantiated");
        }
    }

    public void sendingEmail(SendingEmailMessage sendingEmailMessage) {
        try {
            System.out.println("Envoie d'un email " + (sendingEmailMessage.isInfected() ? "infecte" : "sain"));
            outputStream.writeObject(sendingEmailMessage);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void receivingEmail() throws IOException, ClassNotFoundException {
        messageReceived = (ReceivingEmailMessage) inputStream.readObject();
        if (messageReceived != null) {
            String infectedState = messageReceived.isInfected() ? "infecte" : "sain";
            System.out.println("Email " + infectedState + " reçu de " + messageReceived.getOrigin() + "\n");
        }
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get() && isConnected) {
            try {
                receivingEmail();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (messageReceived != null) {
                isReceivedMessageInfected();
                updateSimulation();
            }
        }
    }

    private void updateSimulation(){
        try {
            simulation.updateStats();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void interrupt() {
        running.set(false);
        if (smtpServerThread != null) {
            smtpServerThread.interrupt();
        }
    }

    private void isReceivedMessageInfected() {
        if (messageReceived.isInfected()) {
            simulation.infectComputer();
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

}