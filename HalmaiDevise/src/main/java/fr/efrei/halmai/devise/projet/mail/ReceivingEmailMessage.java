package fr.efrei.halmai.devise.projet.mail;

import java.io.Serializable;

public class ReceivingEmailMessage implements Serializable {
    private final String origin;
    private final boolean infectedFlag;

    public ReceivingEmailMessage(String origin, boolean isInfected) {
        this.origin = origin;
        this.infectedFlag = isInfected;
    }

    public String getOrigin() {
        return this.origin;
    }

    public boolean isInfected() {
        return this.infectedFlag;
    }
}