package fr.efrei.halmai.devise.projet.service;

public enum Status {

    HEALTHY("Healthy"), INFECTED("Infected"), ISOLATED("Isolated");
    private String value;

    public String getResponse() {
        return value;
    }

    Status(String value){
            this.value = value;
        }

}
