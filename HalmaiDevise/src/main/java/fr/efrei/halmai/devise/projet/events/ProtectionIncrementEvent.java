package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Duration;

public class ProtectionIncrementEvent implements Event {

    private BaseSimulation simulation;


    public ProtectionIncrementEvent(BaseSimulation simulation) {
        this.simulation = simulation;
    }

    @Override
    public void trigger() {
        if (simulation.getMoney() >= 800){
            if (simulation.getProtectionLevel() == 1){
                simulation.setMoney(simulation.getMoney()-800);
                simulation.setProtectionLevel(simulation.getProtectionLevel() + 1);
                System.out.println("Antivirus is now level " + simulation.getProtectionLevel());
            }
            else {
                System.out.println("The protection is already level 2");
            }
        }
        else{
            System.out.println("Insufficient money to upgrade office level");
        }
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }

    public boolean isDurationOk(){
        return true;
    }

}