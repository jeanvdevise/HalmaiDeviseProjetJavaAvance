package fr.efrei.halmai.devise.projet.util;

import fr.efrei.paumier.shared.events.Event;

public class EventSortByDuration implements java.util.Comparator<Event> {

        @Override
        public int compare(Event o1, Event o2) {
            return o1.getDuration().compareTo(o2.getDuration());
        }
}

