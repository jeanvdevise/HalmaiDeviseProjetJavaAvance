package fr.efrei.halmai.devise.projet.engine;

import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.events.*;
import fr.efrei.halmai.devise.projet.mail.SendingEmailMessage;
import fr.efrei.halmai.devise.projet.server.ServerConnection;
import fr.efrei.halmai.devise.projet.service.Status;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.engine.GameEngine;
import fr.efrei.paumier.shared.events.Event;
import fr.efrei.paumier.shared.selection.Selector;
import lombok.Getter;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Getter
public class BaseGame implements GameEngine {

    BaseSimulation simulation;

    private Clock clock;
    private Instant beginning;

    private List<Event> events = new ArrayList<Event>();
    private Instant lastUpdateDate;

    private Selector selector;
    private ServerConnection serverConnection;



    public BaseGame(ServerConnection serverConnection, BaseSimulation simulation, Clock clock) {
        this.clock = clock;
        this.beginning = this.clock.instant();
        this.simulation = simulation;
        this.selector = simulation.getSelector();
        this.serverConnection = serverConnection;
    }

    @Override
    public Instant getCurrentInstant() {
        return this.clock.instant();
    }

    @Override
    public void register(Event... events) {
        for (Event event:events) {
            this.events.add(event);
        }
    }

    @Override
    public void update() {
        registerLoop();
        List<Event> triggeredEventsList = new ArrayList<Event>();
        for (Event event: this.events) {
            if (event.isDurationOk()) {
                event.trigger();
                triggeredEventsList.add(event);
            }
        }
        this.events.removeAll(triggeredEventsList);
        this.lastUpdateDate = getCurrentInstant();
    }

    public void registerLoop(){
        prodEventRegister();
        analyseEventRegister();
        purgeEventRegister();
        EncryptedEventRegister();
    }

    public void prodEventRegister(){
        if (!(this.simulation.isProdEvent())){
            SendingEmailMessage message;
            this.simulation.setProdEvent(true);
            this.register(new ProductionEvent(this.simulation));
            Computer computer = selector.selectAmong(simulation.getComputerList());
            if (computer.getStatus() == Status.HEALTHY){
                message = new SendingEmailMessage(false);
            }
            else {
                message = new SendingEmailMessage(true);
            }
            serverConnection.sendingEmail(message);
        }
    }

    public void analyseEventRegister(){
        for (int i = 0; i<simulation.getProtectionLevel();i++){
            this.register(new AnalyseEvent(this.simulation));
        }
    }

    public void purgeEventRegister(){
        for (Computer c:simulation.getIsolatedComputerList()) {
            if (!c.isPurgeInProgress()){
                c.setPurgeInProgress(true);
                register(new PurgeEvent(this.simulation, c));
            }
        }
    }

    public void EncryptedEventRegister(){
        for (Computer c: simulation.getInfectedComputerList()) {
            if (!c.isEncryptionInProgress() && !c.isComputerEncrypted()){
                c.setEncryptionInProgress(true);
                register(new EncryptedEvent(this.simulation, c));
            }
        }
    }

    public void formattingEventRegister(){
        System.out.println("formattingEventRegister launched");
        this.register(new FormattingEvent(this.simulation));
    }

    public void decipherEventRegister(){
        System.out.println("decipherEventRegister launched");
        this.register(new DecipherEvent(this.simulation));
    }

    public void officeLevelIncrementEventRegister(){
        System.out.println("Attempt to improve office automation level");
        this.register(new OfficeLevelIncrementEvent(this.simulation));
    }

    public void protectionLevelIncrementEventRegister(){
        System.out.println("Attempt to improve antivirus level");
        this.register(new ProtectionIncrementEvent(this.simulation));
    }

}

