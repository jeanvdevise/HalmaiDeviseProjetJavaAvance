package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Duration;

public class OfficeLevelIncrementEvent implements Event {

    private BaseSimulation simulation;


    public OfficeLevelIncrementEvent(BaseSimulation simulation) {
        this.simulation = simulation;
    }

    @Override
    public void trigger() {
        if (simulation.getMoney() >= 50){
            simulation.setMoney(simulation.getMoney()-50);
            simulation.setOfficeLevel(simulation.getOfficeLevel() + 1);
            System.out.println("Office is now level " + simulation.getOfficeLevel());
        }
        else{
            System.out.println("Insufficient money to upgrade office level");
        }
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }

    public boolean isDurationOk(){
        return true;
    }

}
