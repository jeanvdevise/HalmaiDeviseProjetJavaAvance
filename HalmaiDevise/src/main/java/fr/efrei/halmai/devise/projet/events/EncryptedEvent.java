package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.service.Status;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

public class EncryptedEvent implements Event {

    private Clock clock = Clock.systemDefaultZone();
    private Computer computer;
    private Instant beginning;
    private BaseSimulation simulation;
    private long duration = 15;

    public EncryptedEvent(BaseSimulation simulation, Computer computer){
        this.simulation = simulation;
        this.beginning = this.clock.instant();
        this.computer = computer;
    }

    @Override
    public void trigger() {
        if (computer.getStatus() == Status.INFECTED){
            computer.setComputerEncrypted(true);
            this.simulation.getEncryptedComputerList().add(computer);
            computer.setEncryptionInProgress(false);
        }
    }

    public boolean isDurationOk(){
        if (clock.instant().getEpochSecond()-beginning.getEpochSecond()>=this.duration){
            return true;
        }
        return false;
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }
}