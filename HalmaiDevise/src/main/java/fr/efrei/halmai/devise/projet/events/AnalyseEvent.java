package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.service.Status;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Duration;


public class AnalyseEvent implements Event {

    private BaseSimulation simulation;

    public AnalyseEvent(BaseSimulation simulation){
        this.simulation = simulation;
    }

    @Override
    public void trigger() {
        Computer randComputer = simulation.getSelector().selectAmong(simulation.getComputerList());
        if (randComputer.getStatus() == Status.INFECTED){
            this.simulation.getInfectedComputerList().remove(randComputer);
            this.simulation.getIsolatedComputerList().add(randComputer);
            randComputer.setStatus(Status.ISOLATED);
        }
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }

    @Override
    public boolean isDurationOk() {
        return true;
    }
}
