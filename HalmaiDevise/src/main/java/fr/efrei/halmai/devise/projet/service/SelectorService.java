package fr.efrei.halmai.devise.projet.service;

import fr.efrei.paumier.shared.selection.Selector;

import java.util.List;
import java.util.Random;

public class SelectorService implements Selector {
    @Override
    public <TItem> TItem selectAmong(List<TItem> choices) {
        Random rand = new Random();
        return choices.get(rand.nextInt(choices.size()));
    }
}
