package fr.efrei.halmai.devise.projet.simulation;

import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.service.Status;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.simulation.Simulation;
import lombok.Getter;
import lombok.Setter;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class BaseSimulation implements Simulation {

    private boolean ProdEvent = false;

    private Clock clock;
    private Instant beginning;
    private Selector selector;

    private int originalPopulation;
    private int healthyPopulation;
    private int infectedPopulation = 0;
    private int isolatedPopulation = 0;
    private int encryptedPopulation = 0;
    private int money = 0;

    private int officeLevel = 1;
    private int protectionLevel = 1;

    private List<Computer> computerList = new ArrayList<>();
    private List<Computer> healthyComputerList = new ArrayList<>();
    private List<Computer> infectedComputerList = new ArrayList<>();
    private List<Computer> isolatedComputerList = new ArrayList<>();
    private List<Computer> encryptedComputerList = new ArrayList<>();



    public BaseSimulation(Clock clock, Selector selector, int population) {
        for (int i = 0; i<population; i++){
            this.computerList.add(new Computer(i));
        }
        for (Computer c:computerList) {
            healthyComputerList.add(c);
        }
        this.clock = clock;
        this.beginning = this.clock.instant();
        this.selector = selector;
        this.originalPopulation = population;
        this.healthyPopulation = population;
    }

    @Override
    public void update() {

        int peopleToInfect = getInfectedComputerList().size();

        if (peopleToInfect != 0 && getDuration()>=5) {

            for (int i = 0;i<peopleToInfect;i++){
                if (!(this.healthyComputerList.isEmpty())){
                    this.infectComputer();
                }
                else{break;}
            }
            setBeginning(clock.instant());
        }
        if (this.getDuration()>=3 && peopleToInfect == 0) {

            this.infectComputer();
            setBeginning(clock.instant());
        }
    }

    @Override
    public Instant getCurrentInstant() {
        return this.clock.instant();
    }

    public Long getDuration(){
        return clock.instant().getEpochSecond()-beginning.getEpochSecond();
    }

    public void infectComputer(){
        Computer computer = this.selector.selectAmong(this.healthyComputerList);
        this.healthyComputerList.remove(computer);
        computer.setStatus(Status.INFECTED);
        infectedComputerList.add(computer);
    }

    public void updateStats(){
        healthyPopulation = getHealthyComputerList().size();
        infectedPopulation = getInfectedComputerList().size();
        isolatedPopulation = getIsolatedComputerList().size();
        encryptedPopulation = getEncryptedComputerList().size();
    }

    public int encryptedCount(List<Computer> list){
        int encrypted = 0;
        for (Computer c: list) {
            if (c.isComputerEncrypted()){
                encrypted++;
            }
        }
        return encrypted;
    }
}
