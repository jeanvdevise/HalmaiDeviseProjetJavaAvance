package fr.efrei.halmai.devise.projet.entity;

import fr.efrei.halmai.devise.projet.service.Status;



public class Computer {

    private int id;

    private Status status = Status.HEALTHY;

    private boolean isPurgeInProgress = false;
    private boolean isEncryptionInProgress = false;

    private boolean isComputerEncrypted = false;

    public int getId() {
        return id;
    }

    public boolean isPurgeInProgress() {
        return isPurgeInProgress;
    }

    public void setPurgeInProgress(boolean purgeInProgress) {
        isPurgeInProgress = purgeInProgress;
    }

    public boolean isEncryptionInProgress() {
        return isEncryptionInProgress;
    }

    public void setEncryptionInProgress(boolean EncryptionInProgress) {
        isEncryptionInProgress = EncryptionInProgress;
    }

    public boolean isComputerEncrypted() {
        return isComputerEncrypted;
    }

    public void setComputerEncrypted(boolean computerEncrypted) {
        isComputerEncrypted = computerEncrypted;
    }

    public Computer(int id){
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
