package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.service.EventService;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

public class FormattingEvent implements Event {

    private BaseSimulation simulation;
    private EventService eventService;
    private Clock clock = Clock.systemDefaultZone();
    private long duration = 60;
    private Instant beginning = this.clock.instant();;

    public FormattingEvent(BaseSimulation simulation) {
        this.simulation = simulation;
        this.eventService = new EventService(simulation);
    }

    @Override
    public void trigger() {
        eventService.setEncryptedIsolatedComputerHealthy();
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }

    public boolean isDurationOk(){
        if (clock.instant().getEpochSecond()-beginning.getEpochSecond()>=this.duration){
            return true;
        }
        return false;
    }

}
