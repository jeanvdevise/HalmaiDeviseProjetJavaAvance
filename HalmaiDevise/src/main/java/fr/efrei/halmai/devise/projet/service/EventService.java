package fr.efrei.halmai.devise.projet.service;

import fr.efrei.halmai.devise.projet.entity.Computer;
import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;

public class EventService {

    private BaseSimulation simulation;

    public EventService(BaseSimulation simulation){
        this.simulation = simulation;
    }

    public Computer getFirstEncryptedIsolatedComputer(){
        for (Computer isolatedEncryptedComputer: simulation.getIsolatedComputerList()) {
            if (isolatedEncryptedComputer.isComputerEncrypted()){
                return isolatedEncryptedComputer;
            }
        }

        return null;
    }

    public boolean setEncryptedIsolatedComputerHealthy(){
        Computer computer = this.getFirstEncryptedIsolatedComputer();
        if (computer != null){
            simulation.getIsolatedComputerList().remove(computer);
            computer.setStatus(Status.HEALTHY);
            simulation.getEncryptedComputerList().remove(computer);
            computer.setComputerEncrypted(false);
            simulation.getHealthyComputerList().add(computer);
            System.out.println("Computer " + computer.getId() +" has been formatted");
            return true;
        }
        else {
            System.out.println("There is no isolated and encrypted computer to format");
            return false;
        }

    }
}
