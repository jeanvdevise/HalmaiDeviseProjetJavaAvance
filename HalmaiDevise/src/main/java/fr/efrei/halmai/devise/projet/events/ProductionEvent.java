package fr.efrei.halmai.devise.projet.events;

import fr.efrei.halmai.devise.projet.simulation.BaseSimulation;
import fr.efrei.paumier.shared.events.Event;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

public class ProductionEvent implements Event {

    private Clock clock = Clock.systemDefaultZone();
    private Instant beginning;
    private BaseSimulation simulation;
    private long duration = 20;

    public ProductionEvent(BaseSimulation simulation){
        this.simulation = simulation;
        this.beginning = this.clock.instant();
    }

    @Override
    public void trigger() {
        this.simulation.setProdEvent(false);
        this.simulation.setMoney(simulation.getMoney() + (simulation.getHealthyComputerList().size() + (simulation.getInfectedComputerList().size() - simulation.encryptedCount(simulation.getInfectedComputerList()))) * simulation.getOfficeLevel());
    }

    public boolean isDurationOk(){
        if (clock.instant().getEpochSecond()-beginning.getEpochSecond()>=this.duration){
            return true;
        }
        return false;
    }

    @Override
    public Duration getDuration() {
        return null;
    }

    @Override
    public void setDuration(Duration duration) {

    }
}
