extractParam() {
    shopt -s nullglob
    testfiles=(*/target/surefire-reports/*.xml) 
    if [ ${#testfiles[@]} -gt 0 ]; then
        cat */target/surefire-reports/*.xml |grep "<testsuite" | sed -E "s/^.*$1=.([[:digit:]]+).*$/\1/" | awk 'BEGIN { total = 0 } { total += $1 } END { print total }'
    else
        echo 0
    fi
}

total=$(extractParam "tests")
errors=$(extractParam "errors")
skipped=$(extractParam "skipped")
failures=$(extractParam "failures")

successes=$(awk "BEGIN {print $total-$errors-$skipped-$failures; exit}")

mkdir -p tests
echo "Total: $total" > tests/$1.txt
echo "Succès: $successes" >> tests/$1.txt
