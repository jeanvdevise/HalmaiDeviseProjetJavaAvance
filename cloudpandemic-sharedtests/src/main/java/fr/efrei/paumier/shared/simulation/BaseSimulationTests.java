package fr.efrei.paumier.shared.simulation;

import fr.efrei.paumier.shared.categories.Version1;
import fr.efrei.paumier.shared.events.Event;
import fr.efrei.paumier.shared.selection.FakeSelector;
import fr.efrei.paumier.shared.selection.Selector;
import fr.efrei.paumier.shared.time.FakeClock;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public abstract class BaseSimulationTests {

	private FakeSelector selector;
	private FakeClock clock;
	private Simulation simulation;
	
	@Before
	public void setUp() {
		clock = new FakeClock();
		selector = new FakeSelector();
		simulation = createSimulation(clock, selector, 100);
	}
	
	protected List<Event> eventTriggered = new ArrayList<Event>();

	protected abstract Simulation createSimulation(FakeClock clock, Selector selector, int population);

	@Test
	@Category(Version1.class)
	public void starts_everybodyIsHealthy() {
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getHealthyPopulation());
		assertEquals(0, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getIsolatedPopulation());
		assertEquals(0, simulation.getEncryptedPopulation());
	}

	@Test
	@Category(Version1.class)
	public void sec003_onePersonIsInfected() {
		selector.enqueueRanks(0);
		clock.advance(Duration.ofSeconds(3));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getHealthyPopulation());
		assertEquals(1, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getIsolatedPopulation());
		assertEquals(0, simulation.getEncryptedPopulation());
	}

	@Test
	@Category(Version1.class)
	public void sec008_twoPersonsAreInfected() {
		selector.enqueueRanks(0, 0);
		clock.advance(Duration.ofSeconds(8));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getHealthyPopulation());
		assertEquals(2, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getIsolatedPopulation());
		assertEquals(0, simulation.getEncryptedPopulation());
	}

	@Test
	@Category(Version1.class)
	public void sec013_fourPersonsAreInfected() {
		selector.enqueueRanks(0, 0, 0, 0);
		clock.advance(Duration.ofSeconds(13));
		simulation.update();
		
		assertEquals(100, simulation.getOriginalPopulation());
		assertEquals(100, simulation.getHealthyPopulation());
		assertEquals(4, simulation.getInfectedPopulation());
		assertEquals(0, simulation.getIsolatedPopulation());
		assertEquals(0, simulation.getEncryptedPopulation());
	}
	
}
